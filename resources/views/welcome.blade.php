<!doctype html>
<html lang="en">
<head>
    <title>Document</title>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        const baseUrl = "{{url('/')}}";
    </script>
</head>
<body>
@if($errors->has('name'))
    <div class="alert alert-danger">
        <strong>Danger!</strong> {{$errors->first('name')}}
    </div>
@endif
@if($errors->has('email'))
    <div class="alert alert-danger">
        <strong>Danger!</strong> {{$errors->first('email')}}
    </div>
@endif
@if($errors->has('password'))
    <div class="alert alert-danger">
        <strong>Danger!</strong> {{$errors->first('password')}}
    </div>
@endif
@if($errors->has('password_confirmation'))
    <div class="alert alert-danger">
        <strong>Danger!</strong> {{$errors->first('password_confirmation')}}
    </div>
@endif
@if(isset($message))
    <div class="alert alert-success">
        <strong>Danger!</strong> {{$message}}
    </div>
@endif
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Singer
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{route('insert')}}">Insert</a>
                    <a class="dropdown-item" href="{{route('show')}}">Show</a>
                </div>
            </li>
        </ul>
    </div>
    <div class="float-right">
        <button class="btn btn-info" data-toggle="modal" data-target="#sign_in_modal">Sign In</button>
        <button class="btn btn-success" data-toggle="modal" data-target="#sign_up_modal">Sign Up</button>

    </div>
</nav>



@yield('content')

<div class="modal" id="sign_up_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Please Sign Up</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('sign.up')}}" method="post">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name_reg">Your name</label>
                        <input type="text" class="form-control" id="name_reg" name="name">
                    </div>
                    <div class="form-group">
                        <label for="password_reg">Your email</label>
                        <input type="email" class="form-control" id="email_reg" name="email">
                    </div>
                    <div class="form-group">
                        <label for="password_reg">Password</label>
                        <input type="password" class="form-control" id="password_reg" name="password">
                    </div>
                    <div class="form-group">
                        <label for="password_conf_reg">Password Confirmation</label>
                        <input type="password" class="form-control" id="password_conf_reg" name="password_confirmation">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="registration">Sign Up</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="sign_in_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Please Sign In</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="">
                <div class="modal-body">

                    <div class="form-group">
                        <label for="email">Your email</label>
                        <input type="email" class="form-control" id="email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Sign In</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        window.setTimeout(function () {
            $(".alert-danger").alert('close');
            $(".alert-success").alert('close');
        }, 2000);
    });
</script>
</body>
</html>