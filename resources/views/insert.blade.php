@extends('welcome')
@section('content')
    <div class="container-fluid">
        <div class="container">
            <form action="{{route('insert.singer')}}" method="post" enctype="multipart/form-data" class="form-group">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-8">
                        @if($errors->has('first_name'))
                            <span>{{$errors->first('first_name')}}</span><br>
                            @endif
                        <label for="first_name">First Name</label>
                        <input type="text" id="first_name" name="first_name" class="form-control">
                    </div>
                    <div class="col-md-8">
                        @if($errors->has('last_name'))
                            <span>{{$errors->first('last_name')}}</span>
                        @endif
                        <label for="last_name">Last Name</label>
                        <input type="text" id="last_name" name="last_name" class="form-control">
                    </div>
                    <div class="col-md-8">
                        @if($errors->has('age'))
                            <span>{{$errors->first('age')}}</span>
                        @endif
                        <label for="age">Age</label>
                        <input type="number" id="age" name="age" class="form-control">
                    </div>
                    <div class="col-md-8">
                        <label for="image">Image</label>
                        <input type="file" id="image" name="picture" class="form-control">
                    </div>
                    <div class="col-md-8 mt-4">
                        <button type="submit" class="btn-primary">Confirm</button>
                        <button type="reset" class="btn-danger">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection