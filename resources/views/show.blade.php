@extends('welcome')
@section('content')
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                @foreach($singer as $item)
                    <div class="col-md-3 mt-2">
                        @if(is_null($item->picture))
                            <img src="{{asset('image/'.$item->picture)}}" alt="">
                        @endif
                        <h2>{{$item->first_name.' '.$item->last_name}}</h2>
                        <span>{{$item->age}}</span>
                        <span style="float: right;">
                            <i class="material-icons delete" data-id="{{$item->id}}">delete</i>
                            <i class="material-icons update" data-id="{{$item->id}}">update</i>
                        </span>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.delete').on('click', function () {
               var id = $(this).data('id');
                location.replace(baseUrl+'/delete/'+id);
            });
            $('.update').on('click', function () {
                var id = $(this).data('id');
                location.replace(baseUrl+'/update/'+id);
            })
        });
    </script>
@endsection