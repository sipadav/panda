<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class updateSingerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|min:3',
            'last_name'  => 'required|string|max:15',
            'age'        => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'A First name is required',
            'first_name.string'   => 'A First name numeric',
            'first_name.min'      => 'A First name should be maximum 3 characters',
            'last_name.required'  => 'A Last name is required',
            'last_name.string'    => 'A Last name should be string',
            'last_name.min'       => 'A Last name should be maximum 15 characters',
            'age.required'        => 'Age is required',
            'age.numeric'         => 'Age should be integer',
        ];
    }
}
