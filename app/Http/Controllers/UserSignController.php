<?php

namespace App\Http\Controllers;

use App\Http\Requests\registrationRequest;
use App\User;

class UserSignController extends Controller
{
    public function signUpUser(registrationRequest $request)
    {
        $user = User::where('email', $request->input('email'));
        if (!empty($user)){
            return back();
        }
        $newUser = new User;
        $newUser->name     = $request->input('name');
        $newUser->email    = $request->input('email');
        $newUser->password = $request->input('password');
        $newUser->save();
        $message = $request->input('name')." are registered successfully";
        return redirect()->route('show')->with(compact('message'));
        //[$request->input('name')." are registered successfully"]
//        return view('welcome', ["successMessage" => $request->input('name')." are registered successfully"]);
    }
    public function signInUser()
    {

    }
}
