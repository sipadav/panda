<?php

namespace App\Http\Controllers;

use App\Http\Requests\updateSingerRequest;
use Illuminate\Http\Request;
use \Image;
use \Validator;
use App\singer;

class MyController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function insert()
    {
        return view('insert');
    }

    public function show()
    {
       $singer = singer::query()->get();
        return view('show', compact('singer'));
    }

    public function insertSinger(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'first_name' => 'required|string|min:3',
                'last_name' => 'required|string|max:15',
                'age' => 'required|numeric'
            ],[
                'first_name.string'=>'Xndumenq menak tar',
                'first_name.min'=>'Amenaqich@ 6 simvol',
                'last_name.max' => 'maximum 15 simvol',
                'age.numeric' => 'age mast be number',

            ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $fileName = 'null';
        if ($request->hasFile('picture')) {
            $picture = $request->file('picture');
            $extension = $picture->getClientOriginalExtension();
            $fileName = time() . '.' . $extension;
            Image::make($picture)->resize(300, 300)->save(public_path() . '/image/' . $fileName);
        }
        $data = [
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'age' => $request->input('age'),
            'picture' => $fileName
        ];
        Singer::query()->create($data);
        return back();
    }
    public function deleteSinger($id)
    {
        $singer = singer::query()->find($id);
        $success = singer::query()->find($id)->delete();
        if($success){
            if (is_null($singer->picture)) {
                unlink(public_path().'/image/'.$singer->picture);
            }
        }
        return back();
    }
    public function showUpdateView($id)
    {
        if(!is_numeric($id)){
            return $id." is not number";
        }
        $singer = singer::find($id);
        return view('update', ["singer" => $singer]);

    }
    public  function updateSinger(updateSingerRequest $request){
        $fileName = 'null';
        if ($request->hasFile('picture')) {
            $picture = $request->file('picture');
            $extension = $picture->getClientOriginalExtension();
            $fileName = time() . '.' . $extension;
            Image::make($picture)->resize(300, 300)->save(public_path() . '/image/' . $fileName);
        }
        $data = [
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'age' => $request->input('age'),
            'picture' => $fileName
        ];

        $singer = singer::query()->update($data);
        return redirect(route("show"));
    }
}
