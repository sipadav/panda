<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class singer extends Model
{
    protected $table = 'singers';
    protected $fillable = [
        'first_name','last_name', 'age', 'picture'
    ];
}
