<?php

use Illuminate\Support\Facades\Route;


Route::get('/','MyController@index')->name('index');
Route::get('/insert','MyController@insert')->name('insert');
Route::get('/show','MyController@show')->name('show');
Route::post('insert-singer','MyController@insertSinger')->name('insert.singer');
Route::get('/delete/{id}','MyController@deleteSinger');
Route::get('/update/{id}','MyController@showUpdateView');
Route::post('/update','MyController@updateSinger')->name('update.singer');
Route::post('/sign-up','UserSignController@signUpUser')->name('sign.up');